<?php
$I = new AcceptanceTester($scenario);

$I->('a admin');
$I->wantTo('ensure that Laravel works');

//When
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel 5', '.title');
